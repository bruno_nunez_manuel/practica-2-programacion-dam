package main;


public class Butaca {
    private Persona persona;
    private int fila;
    private int columna;

    public Butaca(Persona persona, int fila, int columna) {
        this.persona = persona;
        this.fila = fila;
        this.columna = columna;
    }

    public Persona getPersona() {
        return persona;
    }

    public String toString() {
        return "BUTACA FILA-" + (fila) + " COLUMNA-" + (columna) +
               "\nOCUPANTE: " + persona.toString();
    }

}
