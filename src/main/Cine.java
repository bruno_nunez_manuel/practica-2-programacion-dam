package main;

import java.util.HashMap;


public class Cine {
    private HashMap<Integer,Sala> salas;

    public Cine() {
        this.salas = new HashMap<>();
    }

    public HashMap<Integer, Sala> getSalas() {
        return salas;
    }

    public void anadirSala(int numSala, int filas, int columnas, Pelicula pelicula) {
        salas.put(numSala, new Sala(filas, columnas, pelicula));    }
}
