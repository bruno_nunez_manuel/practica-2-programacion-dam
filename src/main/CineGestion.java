package main;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

// Esta clase probablemente sobre y se pueda hacer en Cine.java pero no sé...
// El principio -OPEN FOR EXTENSION, CLOSED FOR MODIFICATION- me llama...
// El encadenamiento excesivo de métodos reduce la legibilidad, el stack sufre
// de sobrepeso: pero no me arrepiento. Por lo demás, COMPOSITION al poder!
// Pongo los input validadores en esta clase para acotar la UI a la misma,
// ¿habría sido mejor ponerlos cómo static en sus correspondientes clases?
public class CineGestion {
    private final Scanner scanner;
    private final Cine cine;

    public CineGestion() {
        this.scanner = new Scanner(System.in);
        this.cine = new Cine();
        introChula();
    }

    public void iniciar() {
        while (true) {
            System.out.println(
                "\n1. CREAR SALA" +
                "\n2. VENDER ENTRADA" +
                "\n3. VER CANTIDAD DE BUTACAS ASIGNADAS EN SALA" +
                "\n4. VER DATOS DE PERSONA EN BUTACA" +
                "\n5. VER MATRIZ DE BUTACAS EN SALA" +
                "\n6. LIBERAR BUTACA - DEVOLVER ENTRADA" +
                "\n7. LIBERAR TODA LA SALA" +
                "\n8. SALIR"
            );
            switch (scanner.nextLine().trim()) {
                case "1" -> crearSala();
                case "2" -> venderEntrada();
                case "3" -> verButacasAsignadasEnSala();
                case "4" -> verDatosPersonaEnButaca();
                case "5" -> verMatrizButacasDeSala();
                case "6" -> liberarButaca();
                case "7" -> liberarSala();
                case "8" -> salir();
                default -> System.out.println("OPCIÓN INEXISTENTE");
            }
        }
    }

    private void introChula() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(
                    "src/main/introChula.txt"));
            String l;
            while ((l = bufferedReader.readLine()) != null) {
                for (char c : l.toCharArray()) {
                    System.out.print(c);
                    Thread.sleep(1);
                }
                System.out.println();
                Thread.sleep(1);
            }
            bufferedReader.close();
        } catch (IOException | InterruptedException e) {
            System.out.println("No ha cargado la intro, qué pena.");
        }
    }

    private void crearSala() {
        System.out.println("CREANDO SALA...");
        int numSala = nuevaSala();

        System.out.println("Cuántas filas tendrá la nueva sala?");
        int filas = validarIntNatural();

        System.out.println("Cuántas columnas?");
        int columnas = validarIntNatural();

        System.out.println("Introduce el título de la película a proyectar.");
        String titulo = scanner.nextLine().trim();

        System.out.println("Introduce su duración en minutos.");
        int duracion = validarIntNatural();

        System.out.println("Por último, introduce la edad mínima.");
        int edadMinima = validarEdadMinimaPelicula();

        cine.anadirSala(numSala, filas, columnas, new Pelicula(titulo, duracion, edadMinima));
        System.out.println("HAS CREADO LA SALA " + numSala);
    }

    private void venderEntrada() {
        System.out.println("VENDIENDO ENTRADA...");
        int numSala = validarSala();
        int fila = validarFila(numSala);
        int columna = validarColumna(numSala);

        if (cine.getSalas().get(numSala).isButacaLibre(fila, columna)) {
            System.out.println("Introduce Fecha de Nacimiento del cliente en formato DD/MM/AAAA.");
            LocalDate fNacimiento = validarFecha();
            if (cine.getSalas().get(numSala).getPelicula().cumpleEdad(fNacimiento)) {
                System.out.println("Introduce DNI del cliente.");
                String dni = validarDNI();
                System.out.println("Introduce su Nombre.");
                String nombre = scanner.nextLine().trim();
                cine.getSalas().get(numSala).ocuparButaca(
                        fila, columna, new Persona(dni, nombre, fNacimiento));
                System.out.println(
                        "ENTRADA VENDIDA EN:" +
                        "\nSALA-" + numSala + "\n"
                        + cine.getSalas().get(numSala).getButaca(fila,columna).toString()
                );
            } else {
                System.out.println("IMPOSIBLE VENDER, CLIENTE NO CUMPLE EDAD MÍNIMA");
            }
        } else {
            System.out.println("IMPOSIBLE VENDER, BUTACA YA OCUPADA");
        }
    }

    private void verButacasAsignadasEnSala() {
        // Imprimo datos de cada butaca ocupada y finalmente total de butacas ocupadas.
        // Sé que el enunciado no pide lo primero pero me facilita el testeo...
        System.out.println("VIENDO BUTACAS ASIGNADAS EN SALA...");
        int numSala = validarSala();
        System.out.println(cine.getSalas().get(numSala).butacasAsignadas());
    }

    private void verDatosPersonaEnButaca() {
        System.out.println("BUSCANDO DATOS BUTACA...");
        int numSala = validarSala();
        int fila = validarFila(numSala);
        int columna = validarColumna(numSala);
        try {
            System.out.println(cine.getSalas().get(numSala).getButaca(fila,columna).toString());
        } catch (NullPointerException e) {
            System.out.println("ASIENTO SIN ASIGNAR");
        }
    }

    private void verMatrizButacasDeSala() {
        System.out.println("GENERANDO MATRIZ DE BUTACAS DE LA SALA...");
        int numSala = validarSala();
        System.out.println(cine.getSalas().get(numSala).stringMatriz());
    }

    private void liberarButaca() {
        System.out.println("LIBERANDO BUTACA...");
        int numSala = validarSala();
        int fila = validarFila(numSala);
        int columna = validarColumna(numSala);
        if (cine.getSalas().get(numSala).isButacaLibre(fila, columna)) {
            System.out.println("IMPOSIBLE, BUTACA YA LIBRE");
        } else {
            // Aquí no funciona getButaca()?!
            Butaca butaca = cine.getSalas().get(numSala).getMatrizButacas()[fila-1][columna-1];
            System.out.println("La persona con DNI " + butaca.getPersona().getDni() +
                               " ha devuelto su entrada");
            // ref butaca no sirve para nullificar?
            cine.getSalas().get(numSala).getMatrizButacas()[fila-1][columna-1] = null;
            System.out.println("BUTACA LIBERADA");
        }
    }

    private void liberarSala() {
        System.out.println("LIBERANDO SALA ENTERA...");
        int numSala = validarSala();
        Sala sala = cine.getSalas().get(numSala);
        sala.setMatrizButacas(new Butaca[sala.getFilas()][sala.getColumnas()]);
        System.out.println("SALA LIBERADA");
    }

    private void salir() {
        System.out.println("FIN DEL PROGRAMA");
        System.exit(0);
    }


    // INPUT VALIDADORES

    private int validarIntNatural() {
        int num;
        while (true) { // Baja legibilidad? Alternativa do-while con while dentro :(
            try {
                num = Integer.parseInt(scanner.nextLine().trim()); // nextInt() da problemas...
                if (num > 0) break; // La condición demandada primero para poder leer rápido/fácil.
                else System.out.println("Introduce un número entero superior a 0!");
            } catch (NumberFormatException e) {
                System.out.println("Has de introducir un número entero natural.");
            }
        }
        return num;
    }

    private int nuevaSala() {
        int numSala;
        while (true) {
            System.out.println("Introduce número de sala.");
            numSala = validarIntNatural();
            if (!cine.getSalas().containsKey(numSala)) break;
            else {
                System.out.println("Una sala con ese número ya existe. Salas existentes:");
                System.out.println(cine.getSalas().keySet().toString());
                iniciar();
            }
        }
        return numSala;
    }

    private int validarSala() {
        int numSala;
        while (true) {
            System.out.println("Introduce número de sala.");
            numSala = validarIntNatural();
            if (cine.getSalas().containsKey(numSala)) break;
            else {
                System.out.println("No existe tal sala. Salas existentes:");
                System.out.println(cine.getSalas().keySet().toString());
                iniciar();
            }
        }
        return numSala;
    }

    private int validarFila(int numSala) {
        int fila;
        while (true) {
            System.out.println("Introduce número de Fila de la Butaca.");
            fila = validarIntNatural();
            if (fila <= cine.getSalas().get(numSala).getFilas()) break;
            else {
                System.out.println(
                    "\nEl número de fila es superior al de la sala." +
                    "\nFilas de la Sala " + numSala +
                        ": " + cine.getSalas().get(numSala).getFilas());
            }
        }
        return fila;
    }

    private int validarColumna(int numSala) {
        int columna;
        while (true) {
            System.out.println("Introduce número de Columna de la Butaca.");
            columna = validarIntNatural();
            if (columna <= cine.getSalas().get(numSala).getColumnas()) break;
            else {
                System.out.println(
                    "\nEl número de columna es superior al de la sala." +
                    "\nColumnas de la Sala " + numSala +
                        ": " + cine.getSalas().get(numSala).getColumnas());
            }
        }
        return columna;
    }

    private int validarEdadMinimaPelicula() {
        // El hermano tolerante de validarIntNatural()...
        int edadMinima;
        while (true) {
            try {
                edadMinima = Integer.parseInt(scanner.nextLine().trim());
                if (edadMinima >= 0) break;
                else  System.out.println("Introduce un número entero superior o igual a 0!");
            } catch (NumberFormatException e) {
                System.out.println("Has de introducir un número entero superior o igual a 0.");
            }
        }
        return edadMinima;
    }

    private LocalDate validarFecha() {
        // He optado por LocalDate en vez de Date (que parece mediodeprecated).
        LocalDate fNacimiento = null;
        LocalDate hoy = LocalDate.now();
        while (fNacimiento == null || fNacimiento.isAfter(hoy)) {
            try {
                fNacimiento = LocalDate.parse(scanner.nextLine().trim(), Persona.getDateFormatter());
                if (fNacimiento.isAfter(hoy)) { // Los bebés gratis, si es que alguien tiene valor.
                    System.out.println("La fecha de nacimiento no puede ser futura.");
                }
            } catch (DateTimeParseException e) {
                System.out.println("Introduce una fecha correcta DD/MM/AAAA.");
            }
        }
        return fNacimiento;
    }

    private String validarDNI() {
        String dni;
        while(true) {
            dni = scanner.nextLine().trim();
            if (dni.matches("\\d{8}[a-zA-Z]")) break;
            else System.out.println("Introduce un DNI válido en formato 00000000M");
        }
        return dni;
    }

}