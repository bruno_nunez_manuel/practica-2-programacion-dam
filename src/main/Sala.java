package main;


public class Sala {
    private int filas;
    private int columnas;
    private Butaca[][] matrizButacas;
    private Pelicula pelicula;

    public Sala(int filas, int columnas, Pelicula pelicula) {
        this.filas = filas;
        this.columnas = columnas;
        this.matrizButacas = new Butaca[filas][columnas];
        this.pelicula = pelicula;
    }

    public boolean isButacaLibre(int fila, int columna) {
        return matrizButacas[fila-1][columna-1] == null;
    }

    public void ocuparButaca(int fila, int columna, Persona persona) {
        matrizButacas[fila-1][columna-1] = new Butaca(persona, fila, columna);
    }

    public String butacasAsignadas() {
        StringBuilder butacasAsignadas = new StringBuilder();
        int totalButacasAsignadas = 0;
        for (int fila = 0; fila < this.filas; fila++) {
            for (int col = 0; col < this.columnas; col++) {
                if (matrizButacas[fila][col] != null) {
                    butacasAsignadas.append("\n\n").append(matrizButacas[fila][col].toString());
                    totalButacasAsignadas++;
                }
            }
        }
        return butacasAsignadas.toString() +
                "\n\nTOTAL BUTACAS ASIGNADAS: " + totalButacasAsignadas +"/"
                + (filas * columnas);
    }

    public String stringMatriz() {
        StringBuilder matriz = new StringBuilder();
        for (int fila = this.filas-1; fila >= 0; fila--) { // Para imprimir cartesianamente...
            for (int col = 0; col < this.columnas; col++) {
                if (matrizButacas[fila][col] == null) {
                    matriz.append("o ");
                } else matriz.append("+ ");
            }
            matriz.append(" FILA ").append(fila + 1).append("\n");
        }
        for (int i = 0 ; i < this.columnas; i++) {
            if (i == this.columnas/2) {
                matriz.append("PANTALLA");
            } else matriz.append("--");
        }
        return matriz.toString();
    }

    public Butaca getButaca(int fila, int col) {
        return matrizButacas[fila-1][col-1];
    }

    public int getFilas() {
        return filas;
    }

    public int getColumnas() {
        return columnas;
    }

    public Butaca[][] getMatrizButacas() {
        return matrizButacas;
    }

    public Pelicula getPelicula() {
        return pelicula;
    }

    public void setMatrizButacas(Butaca[][] matrizButacas) {
        this.matrizButacas = matrizButacas;
    }
}
