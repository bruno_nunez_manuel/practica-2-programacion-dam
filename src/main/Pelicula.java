package main;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;


public class Pelicula {
    private String titulo;
    private int duracion;
    private int edadMinima;

    public Pelicula(String titulo, int duracion, int edadMinima) {
        this.titulo = titulo;
        this.duracion = duracion;
        this.edadMinima = edadMinima;
    }

    public boolean cumpleEdad(LocalDate fNacimiento) {
        LocalDate hoy = LocalDate.now();
        int edad = (int) ChronoUnit.YEARS.between(fNacimiento, hoy);
        return edad >= edadMinima;
    }

}
