package main;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class Persona {
    private String dni;
    private String nombre;
    private LocalDate fechaNacimiento;
    private static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/uuuu");


    public Persona(String dni, String nombre, LocalDate fechaNacimiento) {
        this.dni = dni;
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
    }

    public static DateTimeFormatter getDateFormatter() {
        return dateFormatter;
    }

    public String getDni() {
        return dni;
    }

    public String toString() {
        return dni + " " + nombre + " " + fechaNacimiento.format(dateFormatter);
    }
}
